import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-pacientes-list',
  templateUrl: './pacientes-list.component.html',
  styleUrls: ['./pacientes-list.component.scss']
})
export class PacientesListComponent implements OnInit {

  public columnas: string[] = [
    "documento",
    "nombre",
    "apellido",
    "sexo",
    "fechaNacimiento"
  ];

  dataSource = new MatTableDataSource();

  constructor() { }

  ngOnInit(): void {
    this.dataSource.data = [
      {
        "id": 1,
        "Nombres": "Pepito",
        "Apellidos": "Pepito",
        "Documento": "112111111",
        "Sexo": "Masculino",
        "FechaNacimiento": "4/10/2020"
      },
        {
        "id": 2,
        "Nombres": "Juanita",
        "Apellidos": "ita",
        "Documento": "112111112",
        "Sexo": "Masculino",
        "FechaNacimiento": "4/10/2020"
      },
        {
        "id": 3,
        "Nombres": "Susanita",
        "Apellidos": "Nita",
        "Documento": "112111113",
        "Sexo": "Masculino",
        "FechaNacimiento": "4/10/2020"
      },
      {
        "id": 4,
        "Nombres": "Carlos",
        "Apellidos": "Pepito",
        "Documento": "112111114",
        "Sexo": "Masculino",
        "FechaNacimiento": "4/10/2020"
      }

    ]
  }

}
