import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PacientesRoutingModule } from './pacientes-routing.module';
import { PacientesListComponent } from './components/pacientes-list/pacientes-list.component';
import { MatButtonModule } from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';


@NgModule({
  declarations: [PacientesListComponent],
  imports: [
    CommonModule,
    PacientesRoutingModule,
    MatButtonModule,
    MatTableModule
  ]
})
export class PacientesModule { }
